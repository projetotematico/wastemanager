﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WasteManager.Models
{
    public enum eResidueType
    {
        Home,
        Industrial,
        Hospital,
        Construction,
        Comercial,
        Radioactive,
        Agricultural,
        Public,
        TransportationPlace
    }

    public enum eRecyclability
    {
       Organic,
       Selective,
       Scrap
    }

	public class Residue
	{
		public int ID { get; set; }
        [EnumDataType(typeof(eResidueType))]
        public eResidueType Type { get; set; }
        [EnumDataType(typeof(eRecyclability))]
        public eRecyclability Recyclability { get; set; }
        public int Quantity { get; set; }
        public int CityID { get; set; }
        public double Cost { get; set; }

        public DateTime Date { get; set; }
	}
}
