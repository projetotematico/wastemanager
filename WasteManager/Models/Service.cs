﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WasteManager.Models;

namespace WasteManager.Models
{
	public class Service
	{
		public int ID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
                   ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

		public int Frequency { get; set; }
		public int ServedPopulation { get; set; }
		public string Name { get; set; }

        public int? ResidueID { get; set; }
        public Residue Residue { get; set; }
       
        public int CityID { get; set; }
    }
}
