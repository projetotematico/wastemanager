﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WasteManager.Models
{
    public class Association
    {
		public string Name { get; set; }
		public int ID { get; set; }
		public int PeopleAssociated { get; set; }
		public int ID_City { get; set; }
	}
}
