﻿using System.ComponentModel;

namespace WasteManager.Models
{
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Population { get; set; }

        [DisplayName("Urban Population")]
        public int UrbanPopulation { get; set; }
    }
}
