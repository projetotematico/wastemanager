﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WasteManager.Models.Context;
using WasteManager.Models;

namespace WasteManager.Models.Context
{
    public class WasteManagerContext : DbContext
    {
        public WasteManagerContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Residue>().ToTable("Residue");
            modelBuilder.Entity<Service>().ToTable("Service");
			modelBuilder.Entity<Association>().ToTable("Association");
        }
        
        public DbSet<City> Cities { get; set; }

        public DbSet<Residue> Residues { get; set; }

        public DbSet<Service> Services { get; set; }

        public DbSet<Association> Association { get; set; }
	}
}
