﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WasteManager.Models.Context
{
	public static class DbInitializer
	{
		public static void Initialize(WasteManagerContext context)
		{
			context.Database.EnsureDeleted();
			context.Database.EnsureCreated();

			if (!context.Cities.Any())
			{

				var cities = new City[]
				{
				new City(){Name = "Antônio Prado", Population = 150000, UrbanPopulation = 95000},
				new City(){Name = "Bento Gonçalves", Population = 200000, UrbanPopulation = 150000},
				new City(){Name = "Carlos Barbosa", Population = 275000, UrbanPopulation = 170000},
				new City(){Name = "Caxias do Sul", Population = 400000, UrbanPopulation = 300000},
				new City(){Name = "Coronel Pilar", Population = 135000, UrbanPopulation = 89000},
				new City(){Name = "Fagundes Varela", Population = 123000, UrbanPopulation = 98000},
				new City(){Name = "Flores da Cunha", Population = 100000, UrbanPopulation = 70000},
				new City(){Name = "Farroupilha", Population = 136000, UrbanPopulation = 86000},
				new City(){Name = "Garibaldi", Population = 230000, UrbanPopulation = 100000},                      new City(){Name = "Nova Roma do Sul", Population = 115000, UrbanPopulation = 56000},
				new City(){Name = "São Marcos", Population = 125000, UrbanPopulation = 76000},
				new City(){Name = "Veranópolis", Population = 230000, UrbanPopulation = 114000},
				};

				foreach (var city in cities)
				{
					context.Cities.Add(city);
				}

				context.SaveChanges();
			}

			var residues = new Residue[]
			{
			new Residue(){Quantity = 10000, Recyclability = eRecyclability.Organic,
				Type = eResidueType.Agricultural, CityID = 4, Cost = 10000.23, Date = DateTime.Now },
			new Residue(){Quantity = 10000, Recyclability = eRecyclability.Selective,
				Type = eResidueType.Home, CityID = 4, Cost = 123431.454, Date = DateTime.Now },
			new Residue(){Quantity = 2452346, Recyclability = eRecyclability.Organic,
				Type = eResidueType.Public, CityID = 5, Cost = 1253.12, Date = DateTime.Now },
			new Residue(){Quantity = 23462, Recyclability = eRecyclability.Selective,
				Type = eResidueType.Comercial, CityID = 5, Cost = 341361.13, Date = DateTime.Now },
			new Residue(){Quantity = 34611, Recyclability = eRecyclability.Scrap,
				Type = eResidueType.Industrial, CityID = 4, Cost = 8171.23, Date = DateTime.Now },
			new Residue(){Quantity = 74643, Recyclability = eRecyclability.Organic,
				Type = eResidueType.Radioactive, CityID = 3, Cost = 98123.13, Date = DateTime.Now },
			new Residue(){Quantity = 654, Recyclability = eRecyclability.Selective,
				Type = eResidueType.Public, CityID = 3, Cost = 3576.13, Date = DateTime.Now },
			new Residue(){Quantity = 47000, Recyclability = eRecyclability.Organic,
				Type = eResidueType.Comercial, CityID = 4, Cost = 6742.15, Date = DateTime.Now },
			new Residue(){Quantity = 12343, Recyclability = eRecyclability.Organic,
				Type = eResidueType.TransportationPlace, CityID = 1, Cost = 22462.32, Date = DateTime.Now },
			new Residue(){Quantity = 53635, Recyclability = eRecyclability.Scrap,
				Type = eResidueType.Home, CityID = 2, Cost = 2743.57, Date = DateTime.Now },
			};

			foreach (var residue in residues)
			{
				context.Residues.Add(residue);
			}

			context.SaveChanges();


			var services = new Service[]
			{
			new Service(){Name = "Varrição de Avenidas", ServedPopulation = 200000, Date = DateTime.Now,
				Frequency = 2, Residue = residues[1], CityID = 1 },
			new Service(){Name = "Capina de Lote", ServedPopulation = 45335, Date = DateTime.Now,
				Frequency = 1, Residue = residues[4], CityID = 2 },
			new Service(){Name = "Movimentação ao aterro sanitário", ServedPopulation = 100000, Date = DateTime.Now,
				Frequency = 7, Residue = residues[5], CityID = 4 },
			new Service(){Name = "Tratamento de Esgoto", ServedPopulation = 25320, Date = DateTime.Now,
				Frequency = 3, Residue = residues[6], CityID = 7 },
			new Service(){Name = "Coleta Seletiva", ServedPopulation = 23440, Date = DateTime.Now,
				Frequency = 5, Residue = residues[9], CityID = 4 },
			new Service(){Name = "Coleta Orgânica", ServedPopulation = 242562, Date = DateTime.Now,
				Frequency = 5, Residue = residues[8], CityID = 3 },
			new Service(){Name = "Conserto de adutora", ServedPopulation = 98000, Date = DateTime.Now,
				Frequency = 0, Residue = residues[3], CityID = 4 },
			new Service(){Name = "Inspeção de Qualidade da Água", ServedPopulation = 245560, Date = DateTime.Now,
				Frequency = 1, Residue = residues[1], CityID = 4 },

			};
			foreach (var service in services)
			{
				context.Services.Add(service);
			}

			context.SaveChanges();


			var associations = new Association[]
			{
			new Association(){Name = "Codeca", PeopleAssociated = 50000, ID_City = 4 },
			new Association(){Name = "Sicoob", PeopleAssociated = 34430, ID_City = 2 },
			new Association(){Name = "Seletec Reciclagem", PeopleAssociated = 50000, ID_City = 4 },
			new Association(){Name = "Braskem", PeopleAssociated = 54430, ID_City = 2 },
			new Association(){Name = "JB Reciclagem", PeopleAssociated = 5230, ID_City = 4 },
			new Association(){Name = "Strasse", PeopleAssociated = 500, ID_City = 4 },
			new Association(){Name = "Eco Shelf", PeopleAssociated = 5200, ID_City = 4 },
			new Association(){Name = "Reciclo", PeopleAssociated = 7600, ID_City = 1 },
			new Association(){Name = "ECO2ME", PeopleAssociated = 4300, ID_City = 1 },
			};

			foreach (var association in associations)
			{
				context.Association.Add(association);
			}

			context.SaveChanges();
		}
	}
}
