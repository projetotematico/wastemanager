﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
    public class ResidueController : Controller
    {
        private readonly WasteManagerContext _context;
        private readonly IGlobalAmbient _globalAmbient;

        public ResidueController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
            this._context = context;
            this._globalAmbient = globalAmbient;
        }

        // GET: Residues
        public async Task<IActionResult> Index()
        {
            var cityGlobalID = this._globalAmbient.GetGlobalCityID();

            return View(await _context.Residues.Where(r => r.CityID == cityGlobalID).ToListAsync());
        }

        // GET: Residues/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var residue = await _context.Residues
                .SingleOrDefaultAsync(m => m.ID == id);
            if (residue == null)
            {
                return NotFound();
            }

            return View(residue);
        }

        // GET: Residues/Create
        public IActionResult Create()
        {
			return View();
        }

        // POST: Residues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Quantity,Type,Recyclability,Cost,Date")] Residue residue)
        {
            if (ModelState.IsValid)
            {
                var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;

                if (cityGlobalId == 0)
                    return RedirectToAction("Index", "Home");

                residue.CityID = cityGlobalId;

                _context.Add(residue);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(residue);
        }

        // GET: Residues/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var residue = await _context.Residues.SingleOrDefaultAsync(m => m.ID == id);
            if (residue == null)
            {
                return NotFound();
            }
            return View(residue);
        }

        // POST: Residues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Quantity,Type,Recyclability,CityID,Cost,Date")] Residue residue)
        {
            if (id != residue.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(residue);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ResidueExists(residue.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(residue);
        }

        // GET: Residues/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var residue = await _context.Residues
                .SingleOrDefaultAsync(m => m.ID == id);
            if (residue == null)
            {
                return NotFound();
            }

            return View(residue);
        }

        // POST: Residues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var residue = await _context.Residues.SingleOrDefaultAsync(m => m.ID == id);
            _context.Residues.Remove(residue);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ResidueExists(int id)
        {
            return _context.Residues.Any(e => e.ID == id);
        }
    }
}
