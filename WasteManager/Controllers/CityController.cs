﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
    public class CityController : Controller
    {
        private readonly WasteManagerContext _context;
		private readonly IGlobalAmbient _globalAmbient;

		public CityController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
            this._context = context;
            this._globalAmbient = globalAmbient;
        }

        // GET: City
        public ActionResult Index()
        {
            return View(this._context.Cities.ToList());
        }

        // GET: City/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: City/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Name, Population, UrbanPopulation")]City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Cities.Add(city);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(city);
        }

        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var cityToUpdate = await this._context.Cities.SingleOrDefaultAsync(c => c.ID == id);
            if (await TryUpdateModelAsync<City>(cityToUpdate, "", c => c.Name, c => c.Population, c => c.UrbanPopulation))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(cityToUpdate);
        }

        // GEt: City/Edit/5
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var cityToEdit = this._context.Cities.Find(id);

            if (cityToEdit == null)
            {
                return RedirectToAction("Index");
            }
            return View(cityToEdit);
        }

        // GET: City/Delete/5
        public ActionResult Delete(int id)
        {
            var citytoDelete = this._context.Cities.Find(id);

            if (citytoDelete == null)
            {
                return RedirectToAction("Index");
            }

            return View(citytoDelete);
        }

        // POST: City/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, City city)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cityToDelete = await _context.Cities
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.ID == id);
            if (cityToDelete == null)
            {
                return NotFound();
            }

            _context.Cities.Remove(city);

            await _context.SaveChangesAsync();

            return Redirect("Index");
        }
    }
}