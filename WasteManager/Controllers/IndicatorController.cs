﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
    public class IndicatorController : Controller
    {

        private readonly WasteManagerContext _context;

        private readonly IGlobalAmbient _globalAmbient;

        public IndicatorController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
            this._context = context;
            this._globalAmbient = globalAmbient;
        }

        // GET: Indicator
        public ActionResult Index()
        {
            return View();
        }
        

        [HttpGet]
        public ActionResult ResidueByPopulation()
        {

            return View();
        }


        [HttpPost]
        public ActionResult ResidueByPopulation([Bind("From, Until")] DateTime From, DateTime Until)
        {
            var globalCityID = this._globalAmbient.GetGlobalCityID();

            var residues = this._context.Residues.Where(r => r.Date.CompareTo(From) > 0 && r.Date.CompareTo(Until) < 0 && r.CityID == globalCityID);

            var population = this._context.Cities.FirstOrDefault(c => c.ID == globalCityID).Population;

            var teste = new Dictionary<eResidueType, double>();

            foreach(var residue in residues)
            {
                if (teste.ContainsKey(residue.Type))
                    teste[residue.Type] += residue.Quantity * 1000 / population;
                else
                    teste.Add(residue.Type, residue.Quantity * 1000 / population);
            }

            return View(teste);
        }

        public ActionResult BiogasByResidue()
        {
            return View();
        }

        public ActionResult ResiduesBYType()
        {
            return View();
        }

        public ActionResult CostsByResidue()
        {
            return View();
        }
    }
}