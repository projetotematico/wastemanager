﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
    public class AssociationController : Controller
    {
        private readonly WasteManagerContext _context;
		private readonly IGlobalAmbient _globalAmbient;
 
        public AssociationController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
            this._context = context;
			this._globalAmbient = globalAmbient;
        }

        // GET: Association
        public async Task<IActionResult> Index()
        {
			var cityGlobalID = this._globalAmbient.GetGlobalCityID();

			return View(await _context.Association.Where(a => a.ID_City == cityGlobalID).ToListAsync());
        }

        // GET: Association/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Association
                .SingleOrDefaultAsync(m => m.ID == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // GET: Association/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Association/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,ID,PeopleAssociated")] Association association)
        {
            if (ModelState.IsValid)
            {
				var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;

				if (cityGlobalId == 0)
					return RedirectToAction("Index", "Home");

				association.ID_City = cityGlobalId;
				this._context.Add(association);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(association);
        }

        // GET: Association/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Association.SingleOrDefaultAsync(m => m.ID == id);
            if (association == null)
            {
                return NotFound();
            }
            return View(association);
        }

        // POST: Association/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,ID,PeopleAssociated")] Association association)
        {
            if (id != association.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(association);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AssociationExists(association.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(association);
        }

        // GET: Association/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var association = await _context.Association
                .SingleOrDefaultAsync(m => m.ID == id);
            if (association == null)
            {
                return NotFound();
            }

            return View(association);
        }

        // POST: Association/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var association = await _context.Association.SingleOrDefaultAsync(m => m.ID == id);
            _context.Association.Remove(association);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssociationExists(int id)
        {
            return _context.Association.Any(e => e.ID == id);
        }
    }
}
