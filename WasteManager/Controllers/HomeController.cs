﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
    public class HomeController : Controller
    {
        private readonly WasteManagerContext _context;

        private readonly IGlobalAmbient _globalAmbient;

        public HomeController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
            this._context = context;
            this._globalAmbient = globalAmbient;
        }

        public IActionResult Index()
        {
            ViewBag.Cities = this._context.Cities.ToList();

            ViewBag.SelectedCityID = this._globalAmbient.GetGlobalCityID();

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult SelectCity(int cityID)
        {
            this._globalAmbient.SetGlobalCityID(cityID);

            return Redirect("Index");
        }
    }
}
