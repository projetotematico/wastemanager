﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WasteManager.Models;
using WasteManager.Models.Context;
using WasteManager.Services;

namespace WasteManager.Controllers
{
	public class ServiceController : Controller
	{
		private readonly WasteManagerContext _context;
        private readonly IGlobalAmbient _globalAmbient;

        public ServiceController(WasteManagerContext context, IGlobalAmbient globalAmbient)
        {
			this._context = context;
            this._globalAmbient = globalAmbient;
        }

		// GET: Services
		public async Task<IActionResult> Index()
        {
            var cityGlobalID = HttpContext.Session.GetInt32("CityID");

            return View(await _context.Services.Include(s => s.Residue).Where(s => s.CityID == cityGlobalID).ToListAsync());
		}



		// GET: Services/Create
		public IActionResult Create()
		{
            var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;
            ViewBag.Residues = this._context.Residues.Where(r => r.CityID == cityGlobalId);

            return View();
		}

		// POST: Services/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("ID,Type,CreationDate,Frequency,ServedPopulation,Name,ResidueID")] Service service)
		{
			try
			{
				if (ModelState.IsValid)
				{
                    var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;

                    ViewBag.Residues = this._context.Residues.Where(r => r.CityID == cityGlobalId);

                    if (cityGlobalId == 0)
                        return RedirectToAction("Index", "Home");

                    service.Residue = this._context.Residues.FirstOrDefault(r => r.ID == service.ResidueID);

                    service.CityID = cityGlobalId;

                    _context.Add(service);
					await _context.SaveChangesAsync();
					return RedirectToAction(nameof(Index));
				}
			}
			catch (DbUpdateException ex)
			{
				ModelState.AddModelError("", "Unable to save changes. " +
				   "Try again, and if the problem persists " +
				   "see your system administrator.");
			}

			return View(service);
		}

		// GET: Services/Edit/5
		[HttpGet]
		public IActionResult Edit(int ID)
		{
            var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;
            ViewBag.Residues = this._context.Residues.Where(r => r.CityID == cityGlobalId);

            var serviceToEdit = this._context.Services.Include(s => s.Residue).FirstOrDefault(s => s.ID == ID);

			if (serviceToEdit == null)
			{
				return RedirectToAction("Index");
			}
			return View(serviceToEdit);
		}

		// POST: Services/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("ID,Type,CreationDate,Frequency,ServedPopulation,Name,Residue,ResidueID,CityID")] Service service)
		{
			if (id != service.ID)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
                    var cityGlobalId = this._globalAmbient.GetGlobalCityID() ?? 0;
                    ViewBag.Residues = this._context.Residues.Where(r => r.CityID == cityGlobalId);

                    service.Residue = this._context.Residues.FirstOrDefault(r => r.ID == service.ResidueID);

                    _context.Update(service);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!ServiceExists(service.ID))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(service);
		}

		// GET: Services/Delete/5
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var service = await _context.Services
				.SingleOrDefaultAsync(m => m.ID == id);
			if (service == null)
			{
				return NotFound();
			}

			return View(service);
		}

		// POST: Services/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var service = await _context.Services.SingleOrDefaultAsync(m => m.ID == id);
			_context.Services.Remove(service);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool ServiceExists(int id)
		{
			return _context.Services.Any(e => e.ID == id);
		}
	}
}
