﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WasteManager.Services
{
    public interface IGlobalAmbient
    {
        int? GetGlobalCityID();

        void SetGlobalCityID(int cityID);
    }
}
