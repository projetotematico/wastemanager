﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WasteManager.Models;

namespace WasteManager.Services
{
    public class GlobalAmbient : Service, IGlobalAmbient
    {
        public GlobalAmbient(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        private readonly IHttpContextAccessor _httpContextAccessor;

        public int? GetGlobalCityID()
        {
            return this._httpContextAccessor.HttpContext.Session.GetInt32("CityID");
        }

        public void SetGlobalCityID(int cityID)
        {
            _httpContextAccessor.HttpContext.Session.SetInt32("CityID", cityID);
        }
    }
}
